
def across(row: str) -> str:
    check = zip(row,row[1:],row[2:])
    out_row = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    for pos,val in enumerate(check):
        if val == ('B','W','W'):
            out_row[pos + 1] = 1
    return out_row

print(across('BWBWWBBWBWBWWWW'))

def down(col: str) -> str:
    check1 = zip(col,col[1:],col[2:])
    out_col = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    for pos,val in enumerate(check1):
        if val == ('B','W','W'):
            out_col[pos + 1] =1
    return out_col

def out(matrix : str) -> str:
    
